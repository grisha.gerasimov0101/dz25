package step_definitions;

import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class DetailPageStepsDef {
    private static final By WISH_LIST_BUTTON = By.xpath("//input [@ class='btn button btn-wishlist']");
    @И("^Добавить продукт в Список пожеланий$")
    public void addWishList() {
        $(WISH_LIST_BUTTON).scrollIntoView(false).click();
    }
}
