package step_definitions;

import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import java.io.File;

import static com.codeborne.selenide.Selenide.$;

public class BookingFormStepsDef {

    private static final By NAME = By.xpath("//input[@ id='firstName']");

    private static final By LAST_NAME = By.xpath("//input[@ id='lastName']");

    private static final By EMAIL = By.xpath("//input[@ id='userEmail']");

    private static final By GENDER = By.xpath("//input[@ id='sex-radio-3']");

    private static final By PHONE = By.xpath("//input[@ id='userNumber']");

    private static final By DATE = By.xpath("//input[@ id='date']");

    private static final By SIZE = By.xpath("//input[@ id='hobbies-checkbox-2']");

    private static final By IMAGE = By.xpath("//input [@ type='file']");

    private static final By SUBMIT_BUTTON = By.xpath("//button [@ id='submit']");

    private static final By ADDRESS = By.xpath("//textarea[@id='currentAddress']");

    @И("^Заполнить форму брони питомца$")
    public void fillOutTheBookingForm() {
        $(NAME).scrollIntoView(false).
                sendKeys("Иван");
        $(LAST_NAME).sendKeys("Иванов");
        $(EMAIL).sendKeys("ivan.i@mail.ru");
        $(GENDER).click();
        $(PHONE).sendKeys("11111111111");
        $(DATE).clear();
        $(DATE).sendKeys("10 Feb 2024");
        $(SIZE).click();
        $(IMAGE).uploadFile(new File("src/main/data/sample.png"));
        $(ADDRESS).sendKeys("г.Москва");
    }
    @И("^Подтвердить отправку$")
    public void submitTheOrderForm() {
    $(SUBMIT_BUTTON).click();
    }
}
