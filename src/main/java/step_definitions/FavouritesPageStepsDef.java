package step_definitions;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class FavouritesPageStepsDef {

    private static final By NAME_PRODUCT = By.xpath("//td[@ class='product_name']//a");

    private static final By COUNT = By.xpath("//span[@ class='qtyval']");

    private static final By DELETE_BUTTON = By.xpath("//img[@ title='Удалить']");

    private static final By EMPTY_LIST = By.xpath("//div[@ class='wishlist_empty_text']");

    @И("^Проверить название продукта \"(.*)\"$")
    public void checkName(String productName) {
        $(NAME_PRODUCT).shouldHave(Condition.text(productName));
    }

    @И("^Проверить количество продуктов \"(.*)\"$")
    public void checkCount(String count) {
        $(COUNT).shouldHave(Condition.text(count));
    }

    @И("^Удалить продукт из Избранного$")
    public void deleteProduct() {
        $(DELETE_BUTTON).click();
        Selenide.switchTo().alert().accept();
    }

    @И("^Убедиться, что Список пожеланий пуст$")
    public void checkDeleteProduct() {
        $(EMPTY_LIST).shouldHave(Condition.text("Ваш список желаний пуст."));
    }

}
