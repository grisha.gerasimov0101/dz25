package step_definitions;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$$;

public class ShopPageStepDef {

    private static final By PRODUCTS_NAME = By.xpath("//div[contains(@class,'product productitem')]/div[@class='name']/a");

    private static final By DETAIL_BUTTON = By.xpath(".//a[contains(@class,'button_detail')]");

    @И("^Выбирать продукт \"(.*)\"$")
    public void selectProduct(String productName) {
        SelenideElement product = $$(PRODUCTS_NAME).findBy(Condition.text(productName)).parent().parent();
        product.$(DETAIL_BUTTON).click();
    }
}