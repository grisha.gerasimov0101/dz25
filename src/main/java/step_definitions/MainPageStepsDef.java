package step_definitions;

import io.cucumber.java.ru.И;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class MainPageStepsDef {

    private static final By ABOUT_US_BUTTON = By.xpath("//ul[@ class='uk-navbar-nav']//a[text()='О нас']");

    private static final By SHOP_BUTTON = By.xpath("//div/preceding-sibling::a[@href='/index.php/magazin']");

    @И("^Открыть страницу$")
    public void openShop() {
        open("https://qahacking.guru/");
    }

    @И("^Перейти во вкладку О нас$")
    public void goAboutUs() {
        $(ABOUT_US_BUTTON).click();
        $(ABOUT_US_BUTTON).click();
    }

    @И("^Перейти во вкладку Магазин$")
    public void goToShop() {
        $(SHOP_BUTTON).click();
        $(SHOP_BUTTON).click();
    }
}
